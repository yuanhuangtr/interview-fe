


// onblue to trigger onchange function reutrn the value
// when min >= max value display error
// 
export default ({onChange, min, max}) => {
    const handleChange=() => {
      onChange(min, max);
    }
    return (<div>
      <input className="min" value={min} onBlur={handleChange}/>
       - 
      <input className="max" value={max} onBlur={handleChange} />
      <div className="error">error</div>
  </div>)
}