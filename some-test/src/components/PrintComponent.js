import { useEffect } from "react";

const PrintComponent = ()=>{

    console.log(1);
    setTimeout(()=>{
        console.log(2);
    });

    useEffect(()=>{
        console.log(3);
    },[]);

    setTimeout(()=>{
        console.log(4);
    },10000)
    return <></>
}

export default PrintComponent;