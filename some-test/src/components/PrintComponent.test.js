import {render} from '@testing-library/react'
import PrintComponent from './PrintComponent'

describe("PrintComponent",()=>{
test("could works",()=>{
    const fn = jest.spyOn(console,"log");
    render(<PrintComponent/>);

    expect(fn).toHaveBeenCalledWith(1);
    expect(fn).toHaveBeenCalledWith(2);
    expect(fn).toHaveBeenCalledWith(3);
    expect(fn).toHaveBeenCalledWith(4);
});

})